<?php
/*
  Template Name: About Template
*/
?>

<?php get_header(); ?>
<div class="slider">
	<?php
	echo do_shortcode('[smartslider3 slider=3]');
	?>
</div>

<div class="container">
	<div class="about-post">
		<?php if(have_posts()) : ?>
	        <?php while(have_posts()) : the_post(); ?>

				<?php the_content(); ?>

	     	<?php endwhile; ?>
	        <?php else : ?>
	           <p><?php __('No Posts Found'); ?></p>
	    <?php endif; ?>
	</div>
</div>


 <?php get_footer(); ?>