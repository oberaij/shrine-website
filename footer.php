 <footer>
    <div class="container">
      <div class="donate-heart">
        <a href=""><img src="<?php bloginfo('template_url'); ?>/assets/images/donate-heart.png" alt=""></a>
      </div>

      <div class="footer-content">
        <h5>Sacred Heart of Jesus Development Corporation</h4>
        <p>Pueblo de Panay, Roxas City, Capiz, Philippines, 5800</p>
        <p>+45 4543 54545</p>
        <br>
        <p>Copyright ©2018 SHJS | <a href="">Privacy Policy</a> | <a href="">Terms of use</a></p>
      </div>
    </div>
  </footer>


      <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
      <script src="js/custom.js"></script>

</body>
</html>