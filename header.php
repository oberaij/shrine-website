<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="<?php bloginfo('charset'); ?>" ">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>
		<?php bloginfo('name'); ?> |
		<?php is_front_page() ? bloginfo('description') : wp_title(); ?>	
	</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/app.css">
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
  <?php wp_head(); ?>

  <style>
    @font-face{
      font-family:'tt_normsmedium';
      src:url("<?php bloginfo('template_url'); ?>/assets/font/tt_norms_medium-webfont.woff2") 
      format("woff2"),url("<?php bloginfo('template_url'); ?>/assets/font/tt_norms_medium-webfont.woff") 
      format("woff");font-weight:normal;font-style:normal
    }
  </style>
</head>
<body>

 <!--  <div class="header"> -->
    
    <div id="main_menu">
      <div class="container">
        <div class="logo_area">
          <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo-1.png" alt=""></a>
        </div>
        <!-- <div class="inner_main_menu">
          <ul>
            <li><a href="">Home</a></li>
            <li><a href="">About</a></li>
            <li><a href="">Latest News</a></li>
            <li><a href="">Events</a></li>
            <li><a href="">Ministries</a></li>
            <li><a href="">Contact us</a></li>
          </ul>
        </div> -->

        <div class="inner_main_menu">
          <?php
	         wp_nav_menu( array(
	             'menu'              => 'primary',
	             'theme_location'    => 'primary',
	             'depth'             => 2,
	             'container'         => 'ul',
	             'container_class'   => '',
	             'container_id'      => '',
	             'menu_class'        => 'inner_main_menu')
	         );
        ?>
        </div>

        
      </div>
    </div>