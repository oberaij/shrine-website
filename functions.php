<?php

function sv_theme_scripts() {
    wp_enqueue_script( 'sv_script', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'sv_theme_scripts' );

// Theme Support
function sv_theme_setup(){

  add_theme_support('post-thumbnails');

  // Nav Menus
  register_nav_menus( array(
          'primary' => __( 'Primary Menu', 'sv_website' ),
          'off-canvass' => __( 'Off-Canvass Menu', 'sv_website' ),
  ) );

  add_theme_support('post-formats', array('aside','gallery'));

}

add_action('after_setup_theme', 'sv_theme_setup');

// Excerpt Length Control
function set_excerpt_length(){
  return 20;
}

add_filter('excerpt_length', 'set_excerpt_length');


// Widget Sidebar
function sv_widgets_init() {
  register_sidebar( array(
      'name' => 'Todays Gospel',
      'id' => 'gospel',
      'description' => 'Todays Gospel',
      'before_widget' => '<p class="blockquote">',
      'after_widget'  => '</p>',
      'before_title'  => '<h5>',
      'after_title'   => '</h5>',
  ));
}

add_action( 'widgets_init', 'sv_widgets_init' );