<?php get_header(); ?>

      <div class="header-text">
        <div class="container text-center">
          <h1>Lorem ipsum dolor <br>sit amet adispising</h1>
          <button class="btn">Read more</button>
        </div>
      </div>

  </div> 

  <div class="donate-banner">
    <div class="container">
          <a href="#" class="btn">Donate now</a>
          <p>We believe that our helping hands guided by the love of <br> Jesus can help heal broken and hurting hearts</p>
    </div>
  </div>

  <div class="categories">
    <div class="container">

      <div class="row">
        <div class="col-md-4 category">
          <img class="img-fluid" src="./images/ministries.png" alt="">
          <a href="#" class="btn">Ministries</a>
        </div>
        <div class="col-md-4 category">
          <img class="img-fluid" src="./images/request.png" alt="">
          <a href="#" class="btn">Prayer Request</a>
        </div>
        <div class="col-md-4 category">
          <img class="img-fluid" src="./images/services.png" alt="">
          <a href="#" class="btn">Services</a>
        </div>
      </div>

    </div>
  </div>

  <div class="latest-news">
    <div class="container">

      <div class="row">
          <div class="col-md-8">
            Latest News
          <hr>

          <div class="row">
            <div class="col-md-6 post-margin">

              <div class="latest-post">
                <img class="img-fluid" src="./images/post-1.png" alt="">
                <div class="title"><a href="">Donec quam felis, ultricies nec</a></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
              </div>

            </div>

            <div class="col-md-6 post-margin">

              <div class="latest-post">
                <img class="img-fluid" src="./images/post-2.png" alt="">
                <div class="title"><a href="">Donec quam felis, ultricies nec</a></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
              </div>
              
            </div>

             <div class="col-md-6 post-margin">

              <div class="latest-post">
                <img class="img-fluid" src="./images/post-2.png" alt="">
                <div class="title"><a href="">Donec quam felis, ultricies nec</a></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
              </div>
              
            </div>

            <div class="col-md-6 post-margin">

              <div class="latest-post">
                <img class="img-fluid" src="./images/post-2.png" alt="">
                <div class="title"><a href="">Donec quam felis, ultricies nec</a></div>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
              </div>
              
            </div>
          </div> <!-- Inner div for post -->
        </div> <!-- Outder div for post and sidebar Div Col 8-->

        
        <div class="col-md-4">
          <div class="sidebar">
            More coming events
          <hr>
          <div class="events">
            <div class="row">
              <div class="col-md-2">
                  <div class="date">
                  <div class="day">6</div>
                  <div class="month">Nov</div>
                </div>
              </div>

              <div class="col-md-7">
                 <div class="event-name">
                  Monday Prayer
                  <small>Wednesday / November7, 2019</small><br>
                  <small>7:30 PM</small>
                </div>
              </div>

              <div class="col-md-3">
                <a href="#" class="btn">Details</a>
              </div>
            </div>

            <hr>
          </div>

           <div class="events">
            <div class="row">
              <div class="col-md-2">
                 <div class="date">
                  <div class="day">8</div>
                  <div class="month">Nov</div>
                </div>
              </div>

              <div class="col-md-7">
                 <div class="event-name">
                  Monday Prayer
                  <small>Wednesday / November7, 2019</small><br>
                  <small>7:30 PM</small>
                </div>
              </div>

              <div class="col-md-3">
                <a href="#" class="btn">Details</a>
              </div>
            </div>
            <hr>
          </div> 

          <div class="todays-gospel">
            Today's Gospel
            <hr>

            <p class="blockquote">Tax collectors and sinners were all drawing near to listen to Jesus,
but the Pharisees and scribes began to complain, saying, "This man welcomes sinners and eats with them."

So to them he addressed this parable. <br><br>

"What man among you having a hundred sheep and losing one of them would not leave the ninety-nine in the desert and go after the lost one until he finds it?

And when he does find it, he sets it on his shoulders with great joy and, upon his arrival home, he calls together his friends and neighbors and says to them, 'Rejoice with me because I have found my lost sheep.'<br><br>

I tell you, in just the same way there will be more joy in heaven over one sinner who repents than over ninety-nine righteous people who have no need of repentance."

Or what woman having ten coins and losing one would not light a lamp and sweep the house, searching carefully until she finds it?<br><br>

And when she does find it, she calls together her friends and neighbors and says to them, 'Rejoice with me because I have found the coin that I lost.'

In just the same way, I tell you, there will be rejoicing among the angels of God over one sinner who repents."</p>
          </div>

          </div><!-- Sidebar -->
        </div>

      </div>
    </div> <!-- End container -->
      
  </div> <!-- End latest news -->

  <div class="location">
    <div class="container"> 
        <div class="row">
          <div class="col-md-5">
            <div class="address">
              <h5>Radiating the joy of the gospel. In the heart of Roxas City</h5>
              <br>
              <h5>Address</h5>
              <p>Pueblo de Panay, Roxas City, Capiz, Philippines, 5800</p>
              <br>
              <h5>Telefax</h5>
              <p>(632) 570 9220</p>
              <br>
              <h5>Email</h5>
              <p>parish_shj@gmail.com</p>
              <hr>
              <h5>Church Hours</h5>
              <p>We are blessed to be able to leave the church open during the day for you to stop by for prayer</p>
              <br>
              <h5>Sunday</h5>
              <p>4:30 am - 8:00 pm</p>
              <h5>Monday - Friday</h4>
            </div>
          </div>
          <div class="col-md-7">
            <img class="img-fluid" src="./images/Map.png" alt="">
          </div>
        </div>
    </div>
  </div>


  <footer>
    <div class="container">
      <div class="donate-heart">
        <a href=""><img src="./images/donate-heart.png" alt=""></a>
      </div>

      <div class="footer-content">
        <h5>Sacred Heart of Jesus Development Corporation</h4>
        <p>Pueblo de Panay, Roxas City, Capiz, Philippines, 5800</p>
        <p>+45 4543 54545</p>
        <br>
        <p>Copyright ©2018 SHJS | <a href="">Privacy Policy</a> | <a href="">Terms of use</a></p>
      </div>
    </div>
  </footer>


      <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
      <script src="js/custom.js"></script>

</body>
</html>
