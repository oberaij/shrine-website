<?php get_header(); ?>
	<div class="slider">
		<?php
		echo do_shortcode('[smartslider3 slider=2]');
		?>
	</div>
	 
     <!-- <div class="header-text">
        <div class="container text-center">
          <h1>Lorem ipsum dolor <br>sit amet adispising</h1>
          <button class="btn">Read more</button>
        </div>
      </div> -->

  <div class="donate-banner">
    <div class="container">
          <a href="#" class="btn">Donate now</a>
          <p>We believe that our helping hands guided by the love of <br> Jesus can help heal broken and hurting hearts</p>
    </div>
  </div>

  <div class="categories">
    <div class="container">

      <div class="row">
        <div class="col-md-4 category">
          <img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/ministries.png" alt="">
          <a href="#" class="btn">Ministries</a>
        </div>
        <div class="col-md-4 category">
          <img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/request.png" alt="">
          <a href="#" class="btn">Prayer Request</a>
        </div>
        <div class="col-md-4 category">
          <img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/services.png" alt="">
          <a href="#" class="btn">Services</a>
        </div>
      </div>

    </div>
  </div>

  <div class="latest-news">
    <div class="container">

      <div class="row">
          <div class="col-md-8">
            Latest News
          <hr>

          <div class="row">
          	 <?php
                query_posts('posts_per_page=4'); /*1, 2*/

                if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
            	<div class="col-md-6 post-margin">

	              	<div class="latest-post">
		                <?php if(has_post_thumbnail()) : ?>
	                            <?php the_post_thumbnail();?>
	                    <?php endif; ?>
	                <div class="title"><a href="<?php the_permalink(); ?>"><?php the_title();/*3*/ ?></a></div>
		                <p>
		                	<?php the_time('g:i a'); ?> |
		                	<?php the_time('F j, Y'); ?>
		            	</p>
	                <p><?php the_excerpt(15); ?></p>
	              	</div>

            	</div>
             <?php endwhile; ?> <?php wp_reset_query(); /*4*/ ?>

          </div> <!-- Inner div for post -->
        </div> <!-- Outder div for post and sidebar Div Col 8-->

        
        <div class="col-md-4">
          <div class="sidebar">
            More coming events
          <hr>
          <div class="events">
            <div class="row">
              <div class="col-md-2">
                  <div class="date">
                  <div class="day">6</div>
                  <div class="month">Nov</div>
                </div>
              </div>

              <div class="col-md-7">
                 <div class="event-name">
                  Monday Prayer
                  <small>Wednesday / November7, 2019</small><br>
                  <small>7:30 PM</small>
                </div>
              </div>

              <div class="col-md-3">
                <a href="#" class="btn">Details</a>
              </div>
            </div>

            <hr>
          </div>

           <div class="events">
            <div class="row">
              <div class="col-md-2">
                 <div class="date">
                  <div class="day">8</div>
                  <div class="month">Nov</div>
                </div>
              </div>

              <div class="col-md-7">
                 <div class="event-name">
                  Monday Prayer
                  <small>Wednesday / November7, 2019</small><br>
                  <small>7:30 PM</small>
                </div>
              </div>

              <div class="col-md-3">
                <a href="#" class="btn">Details</a>
              </div>
            </div>
            <hr>
          </div> 

          <div class="todays-gospel">
            Today's Gospel
            <hr>

            <?php if(is_active_sidebar('gospel')) : ?>
                <?php dynamic_sidebar('gospel'); ?>
            <?php endif; ?>

          </div>

          </div><!-- Sidebar -->
        </div>

      </div>
    </div> <!-- End container -->
      
  </div> <!-- End latest news -->

  <div class="location">
    <div class="container"> 
        <div class="row">
          <div class="col-md-5">
            <div class="address">
              <h5>Radiating the joy of the gospel. In the heart of Roxas City</h5>
              <br>
              <h5>Address</h5>
              <p>Pueblo de Panay, Roxas City, Capiz, Philippines, 5800</p>
              <br>
              <h5>Telefax</h5>
              <p>(632) 570 9220</p>
              <br>
              <h5>Email</h5>
              <p>parish_shj@gmail.com</p>
              <hr>
              <h5>Church Hours</h5>
              <p>We are blessed to be able to leave the church open during the day for you to stop by for prayer</p>
              <br>
              <h5>Sunday</h5>
              <p>4:30 am - 8:00 pm</p>
              <h5>Monday - Friday</h4>
            </div>
          </div>
          <div class="col-md-7">
            <div class="map"></div>
          </div>
        </div>
    </div>
  </div>


 <?php get_footer(); ?>